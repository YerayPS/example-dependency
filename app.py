import json

from connection import app
from database.DataModels.userModel import User


@app.route("/", methods=['GET', 'POST'])
def home():
    data = User.selectAllPaginated(1)
    return json.dumps(data), 200


if __name__ == "__main__":
    app.run(debug=False, port=80)
